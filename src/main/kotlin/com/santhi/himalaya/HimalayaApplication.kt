package com.santhi.himalaya

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HimalayaApplication

fun main(args: Array<String>) {
    runApplication<HimalayaApplication>(*args)
}
